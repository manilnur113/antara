--1.         Вывести список самолетов с кодами 320, 321, 733;
select * from aircrafts a  
where a.aircraft_code in ('320','321','733');

--2.Вывести список самолетов с кодом не на 3;
select * from aircrafts a 
where aircraft_code not like '3%%';

--3.        Найти билеты оформленные на имя «OLGA», с емайлом «OLGA» или без емайла;
SELECT *
FROM tickets
WHERE passenger_name like 'OLGA%'
AND (contact_data->>'email' = 'OLGA' 
OR NOT contact_data ? 'email');

--4.         Найти самолеты с дальностью полета 5600, 5700. Отсортировать список по убыванию дальности полета;
select * from aircrafts a 
where range in(5600,5700)
order by range desc;

--5.         Найти аэропорты в Moscow. Вывести название аэропорта вместе с городом. Отсортировать по полученному названию;
select  airport_name, city  from airports a 
where a.city = 'Moscow'-- в задании указано найти именно Moscow а не Москва поэтому запрос не выдаст результатов
order by airport_name ;

--6.         Вывести список всех городов без повторов в зоне «Europe»;
select distinct city   from airports a 
where timezone like 'Europe%';

--7.         Найти бронирование с кодом на «3A4» и вывести сумму брони со скидкой 10%
select book_ref, total_amount*0.9 as new_amount  from bookings b 
where book_ref like '3A4%';

--8.         Вывести все данные по местам в самолете с кодом 320 и классом «Business» строками вида «Данные по месту: номер места 1», «Данные по месту: номер места 2» … и тд
SELECT CONCAT('Данные по месту: номер места ', seat_no) AS seatinfo 
FROM seats 
WHERE aircraft_code ='320' AND fare_conditions ='Business';

--9.         Найти максимальную и минимальную сумму бронирования в 2017 году;
-- запрос будет не рабочий так как в таблице данные максимум за 2016й. тут и далее сделаем за 2016й-год:
select max(total_amount), min(total_amount)  from bookings b  
where b.book_date between '2016-01-01 00:00:00.000 +0500' and  '2016-12-31 23:59:59.999 +0500';
--max(total_amount), min(total_amount)

--10.      Найти количество мест во всех самолетах, вывести в разрезе самолетов;
select aircraft_code, count(seat_no)  from seats s 
group by aircraft_code;
--11.      Найти количество мест во всех самолетах с учетом типа места, вывести в разрезе самолетов и типа мест;
select aircraft_code, fare_conditions, count(seat_no)  from seats s 
group by aircraft_code, fare_conditions
order by aircraft_code ;

--12.      Найти количество билетов пассажира ALEKSANDR STEPANOV, телефон которого заканчивается на 11;
select count(book_ref) from tickets t 
where passenger_name = 'ALEKSANDR STEPANOV'
and (contact_data->>'phone' like '%11');

--13.      Вывести всех пассажиров с именем ALEKSANDR, у которых количество билетов больше 2000. Отсортировать по убыванию количества билетов;
select passenger_name  from tickets t 
where passenger_name like 'ALEKSANDR%'
group by passenger_name
having ( count(book_ref) > 2000)
order by count(book_ref) desc;

--14.      Вывести дни в сентябре 2016 с количеством рейсов больше 500. 

select date_trunc('month',scheduled_departure)  from flights f 
where scheduled_departure  between '2016-01-01 00:00:00.000 +0500' and  '2016-12-31 23:59:59.999 +0500'
group by date_trunc 
having (count(flight_id)>500)


--15.      Вывести список городов, в которых несколько аэропортов
select city from airports a 
group by a.city 
having (count(airport_code)>1);
--16.      Вывести модель самолета и список мест в нем, т.е. на самолет одна строка результата
SELECT a.model, STRING_AGG(s.seat_no, ', ') as seats
FROM aircrafts a
LEFT JOIN seats s ON a.aircraft_code = s.aircraft_code
GROUP BY a.model;

--17.      Вывести информацию по всем рейсам из аэропортов в г.Москва за сентябрь 2017
select *  from flights f 
where f.actual_arrival  between '2016-01-01 00:00:00.000 +0500' 
and '2016-12-31 23:59:59.999 +0500' 
and departure_airport  in (select airport_code  from airports a where a.city = 'Москва');

--18.      Вывести кол-во рейсов по каждому аэропорту в г.Москва за 2017
SELECT a.airport_name, COUNT(f.flight_id) 
FROM flights f 
JOIN airports a ON f.departure_airport = a.airport_code or f.arrival_airport = a.airport_code 
WHERE f.actual_arrival BETWEEN '2016-01-01 00:00:00.000 +0500' AND '2016-12-31 23:59:59.999 +0500' 
AND a.city = 'Москва'
GROUP BY a.airport_name;

--19.      Вывести кол-во рейсов по каждому аэропорту, месяцу в г.Москва за 2017
SELECT a.airport_name, COUNT(*) AS flight_count, EXTRACT(month  FROM f.scheduled_departure) AS month
FROM flights f
JOIN airports a ON f.departure_airport = a.airport_code OR f.arrival_airport = a.airport_code
WHERE f.scheduled_departure BETWEEN '2016-01-01 00:00:00' AND '2016-12-31 23:59:59'
  AND a.city = 'Москва'
GROUP BY a.airport_name, month
ORDER BY month, a.airport_name;

--20.      Найти все билеты по бронированию на «3A4B»
select ticket_no  from tickets t 
where t.book_ref like '3A4B%';
--21.      Найти все перелеты по бронированию на «3A4B»
select flight_id  from ticket_flights tf 
left join tickets t on t.ticket_no = tf.ticket_no 
where t.book_ref like '3A4B%'