package src.main.java;

import animals.Animal;
import animals.Voice;
import food.Food;

public class Worker {

    private String name;

    public Worker(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void feed(Animal animal, Food food) {
        animal.eat(food);
    }

    public <T extends Animal & Voice> void getVoice(T animal) {
        System.out.println(animal.voice());
    }
}