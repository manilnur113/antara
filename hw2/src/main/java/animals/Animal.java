package animals;

import food.Food;

public abstract class Animal {
    private String name;
    private int satietyLevel = 1;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getSatietyLevel() {
        return satietyLevel;
    }

    public void setSatietyLevel(int satiety) {
        this.satietyLevel += satiety;
    }


    public abstract void eat(Food food);
}