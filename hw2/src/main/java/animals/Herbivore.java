package animals;

import food.Food;
import food.Grass;

public class Herbivore extends Animal {


    public Herbivore(String name) {
        super(name);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Grass) {
            if (this.getSatietyLevel() > 5)
                System.out.println(getName() + " Наелся");
            else {
                System.out.println(getName() + " ест " + food.getName());
                this.setSatietyLevel(food.getSatiety());
            }

        } else {
            System.out.println(getName() + " отказывается есть " + food.getName());
        }
    }
}
