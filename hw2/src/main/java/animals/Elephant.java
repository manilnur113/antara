package animals;

public class Elephant extends Herbivore implements Voice, Run {


    public Elephant(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println(getName() + " Медленно тяжело идет");

    }

    @Override
    public String voice() {
        return "Трубит хоботом";
    }
}