package animals;

public class Tiger extends Carnivorous implements Voice, Run {


    public Tiger(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println(getName() + " Бежит");
    }

    @Override
    public String voice() {
        return "Рычит";
    }
}