package animals;


public class Duck extends Herbivore implements Fly, Voice, Swim {


    public Duck(String name) {
        super(name);
    }

    @Override
    public void fly() {
        System.out.println(getName() + " летит");
    }

    @Override
    public String voice() {
        return "Кря Кря";
    }

    @Override
    public void swim() {
        System.out.println(getName() + " плавает");
    }
}


