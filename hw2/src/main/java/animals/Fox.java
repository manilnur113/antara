package animals;

public class Fox extends Carnivorous implements Voice, Run {


    public Fox(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println(getName() + " Убегает");
    }

    @Override
    public String voice() {
        return "Фыркает и мурлычет";
    }
}
