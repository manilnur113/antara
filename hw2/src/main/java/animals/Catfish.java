package animals;

public class Catfish extends Carnivorous implements Swim {


    public Catfish(String name) {
        super(name);
    }

    @Override
    public void swim() {
        System.out.println(getName() + " плавает");
    }
}
