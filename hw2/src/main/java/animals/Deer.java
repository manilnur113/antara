package animals;

public class Deer extends Herbivore implements Voice, Run {

    public Deer(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println(getName() + " Пугливо убегает");

    }

    @Override
    public String voice() {
        return "МООООУУУУУУУ";
    }
}
