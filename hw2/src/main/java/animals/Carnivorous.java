package animals;

import food.Food;
import food.Grass;
import food.Meat;

public class Carnivorous extends Animal {


    public Carnivorous(String name) {
        super(name);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Meat) {
            if (this.getSatietyLevel() > 5)
                System.out.println(getName() + " Наелся");
            else {
                System.out.println(getName() + " ест " + food.getName());
                this.setSatietyLevel(food.getSatiety());
            }

        } else {
            System.out.println(getName() + " отказывается есть " + food.getName());
        }
    }
}

