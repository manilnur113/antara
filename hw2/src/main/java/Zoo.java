package src.main.java;

import animals.Duck;
import animals.Swim;
import animals.Deer;
import animals.Elephant;
import animals.Catfish;
import animals.Fox;
import animals.Tiger;

import food.Grass;
import food.Meat;

import java.util.ArrayList;


public class Zoo {
    public static void main(String[] args) {
        Worker worker = new Worker("Олег");
        Duck duck1 = new Duck("Билли");
        Duck duck2 = new Duck("Вилли");
        Duck duck3 = new Duck("Дилли");
        Deer deer = new Deer("Рудольф");
        Elephant elephant = new Elephant("Малыш");
        Catfish catfish = new Catfish("Усач");
        Fox fox1 = new Fox("Ник");
        Fox fox2 = new Fox("Лу");
        Tiger tiger = new Tiger("Амур");

        Grass grass = new Grass("корм для травоядных", 2);
        Meat meat = new Meat("мясо", 3);

        ArrayList<Swim> pond = new ArrayList<>();
        pond.add(duck1);
        pond.add(duck2);
        pond.add(duck3);
        pond.add(catfish);

        worker.feed(duck1, grass);
        worker.feed(duck1, grass);
        worker.feed(duck1, grass);
        worker.feed(duck1, grass);
        worker.feed(duck2, meat);
        worker.feed(deer, grass);
        worker.feed(elephant, meat);
        worker.feed(tiger, grass);
        worker.getVoice(duck2);
        worker.getVoice(deer);

        tiger.run();
        fox1.run();

        for (Swim swimmer : pond) {
            swimmer.swim();
        }

    }
}
