package food;

public abstract class Food {
    private String name;
    private int satiety;

    public Food(String name, int satiety) {
        this.name = name;
        this.satiety =satiety;
    }

    public String getName() {
        return name;
    }
    public int getSatiety(){
        return satiety;
    }
}

