import model.Kotik;

public class Application {
    public static void main(String[] args) {

        Kotik kotik1 = new Kotik(10, "Морис", 5, "Мааау!", 1);
        Kotik kotik2 = new Kotik();
        kotik2.setKotik(7, "Gato", 3, "Мяу.", 5);

        kotik1.liveAnotherDay();

        System.out.println("Имя: " + kotik1.getName());
        System.out.println("Вес: " + kotik1.getWeight());
        System.out.println("Звук: " + kotik1.getMeow());

        System.out.println("Имя: " + kotik2.getName());
        System.out.println("Вес: " + kotik2.getWeight());
        System.out.println("Звук: " + kotik2.getMeow());

        if (kotik1.getMeow().equals(kotik2.getMeow())) {
            System.out.println("Звук одинаковый");
        } else {
            System.out.println("Звук разный");
        }

        System.out.println("Количество ваших котиков: " + Kotik.getTotalCount());
    }
}
