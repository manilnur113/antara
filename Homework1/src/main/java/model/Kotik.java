package model;

import java.util.Random;

public class Kotik {

    private int prettiness;
    private String name;
    private int weight;
    private String meow;
    private int satietyLevel;
    private static int totalCount;

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setSatietyLevel(int satietyLevel) {
        this.satietyLevel = satietyLevel;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    public int getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    public String getMeow() {
        return meow;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public int getSatietyLevel() {
        return satietyLevel;
    }

    public static int getTotalCount() {
        return totalCount;
    }

    public Kotik(int prettiness, String name, int weight, String meow, int satietyLevel) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        this.satietyLevel = satietyLevel;
        totalCount++; // Увеличиваем счетчик созданных объектов
    }

    public void setKotik(int prettiness, String name, int weight, String meow, int satietyLevel) {
        setPrettiness(prettiness);
        setName(name);
        setWeight(weight);
        setMeow(meow);
        setSatietyLevel(satietyLevel);
    }

    public Kotik() {
        totalCount++; // Увеличиваем счетчик созданных объектов
    }


    public boolean play() {
        if (satietyLevel <= 0) {
            System.out.println("Я голоден, не буду играть!");
            return false;
        }
        System.out.println("Поиграл с игрушками!");
        satietyLevel--;
        return true;
    }

    public boolean sleep() {
        if (satietyLevel <= 0) {
            System.out.println("Не могу спать, я голоден!");
            return false;
        }
        System.out.println("Ушел спать...");
        satietyLevel--;
        return true;
    }

    public boolean chaseMouse() {
        if (satietyLevel <= 0) {
            System.out.println("Нет сил ловить мышей, я голоден!");
            return false;
        }
        System.out.println("Поймал мышь!");
        satietyLevel--;
        return true;
    }

    public boolean helpOwner() {
        if (satietyLevel <= 0) {
            System.out.println("Я бы помог хозяину с работой, но я хочу есть");
            return false;
        }
        System.out.println("Побегал по клавитуре");
        satietyLevel--;
        return true;
    }

    public boolean climbCurtain() {
        if (satietyLevel <= 0) {
            System.out.println("такая высота не покорится мне пока я голоден");
            return false;
        }
        System.out.println("Залез на штору");
        satietyLevel--;
        return true;
    }

    public void eat(int food) {
        satietyLevel += food;
        System.out.println("Поел " + food + " порций еды.");
    }

    public void eat(int food, String foodName) {
        satietyLevel += food;
        System.out.println("Поел " + food + " порций " + foodName + ".");
    }

    public void eat() {
        eat(1, "корма"); // Вызываем перегруженный метод eat(int food, String foodName) с значениями по умолчанию
    }

    public void liveAnotherDay() {
        Random random = new Random();
        int actions = 24;

        for (int i = 0; i < actions; i++) {
            int behavior = random.nextInt(7) + 1;
            switch (behavior) {
                case 1:
                    if (!play()) {
                        eat();
                        actions--;
                    }
                    break;
                case 2:
                    if (!sleep()) {
                        eat();
                        actions--;
                    }
                    break;
                case 3:
                    if (!chaseMouse()) {
                        eat();
                        actions--;
                    }
                    break;
                case 4:
                    if (!helpOwner()) {
                        eat();
                        actions--;
                    }
                    break;
                case 5:
                    if (!climbCurtain()) {
                        eat();
                        actions--;
                    }
                    break;
                case 6:
                    eat();
                    break;
                case 7:
                    eat(random.nextInt(3) + 1);
                    break;
            }
        }
    }

}

